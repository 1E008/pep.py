FROM python:3.6-slim

WORKDIR /opt/pep.py
ADD . /opt/pep.py

ENV mysql_usr = "rippleuser" \
        peppy_cikey = "changeme" \
        mysql_psw = "secrez"

RUN apt-get update && apt-get install -y git python-dev mysql-server default-libmysqlclient-dev gcc
RUN git submodule update --init --recursive

# Install any needed packages specified in requirements.txt
RUN pip3 install --trusted-host pypi.python.org -r requirements.txt

RUN python3.6 setup.py build_ext --inplace
RUN python3.6 pep.py
RUN sed -i "s#root#'${mysql_usr}'#g; s#changeme#'${peppy_cikey}'#g" /opt/pep.py/config.ini
RUN sed -E -i -e 'H;1h;$!d;x' /opt/pep.py/config.ini -e "s#password = #password = '${mysql_psw}'#"

# redis server irc
EXPOSE 6379 5001 6667

# Run app.py when the container launches
CMD ["python3.6", "pep.py"]
